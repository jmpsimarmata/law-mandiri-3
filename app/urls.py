from django.urls import path
from app.views import echo_message

app_name = 'mata_kuliah'
urlpatterns = [
     path('', echo_message, name="echo_message"),
]
