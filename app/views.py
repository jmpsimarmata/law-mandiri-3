from django.shortcuts import render
import logging
from logstash_async.handler import AsynchronousLogstashHandler
from logstash_async.handler import LogstashFormatter
from django.views.decorators.http import require_http_methods
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status

# Create the logger and set it's logging level
logger = logging.getLogger("logstash")
logger.setLevel(logging.DEBUG)        

# Create the handler
handler = AsynchronousLogstashHandler(
    host='a9de4e66-2696-47bb-8327-b817e3f6f350-ls.logit.io', 
    port=30768, 
    database_path='')

# Additional formatting on your log record/message
formatter = LogstashFormatter()
handler.setFormatter(formatter)

# Assign handler to the logger
logger.addHandler(handler)

@require_http_methods(["GET"])
@api_view(['GET'])
def echo_message(request):
     logger.debug(str({
          'headers': request.headers,
          'query_params' : request.query_params,
          'method': request.method,
          'host' : request.get_host(),
          'full_path': request.build_absolute_uri(),
     }))
     response_data  = {'data': request.query_params}
     return Response(response_data, status=status.HTTP_200_OK)



